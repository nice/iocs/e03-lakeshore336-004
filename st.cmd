require essioc
require lakeshore336

epicsEnvSet("IPADDR",               "172.30.32.30")
epicsEnvSet("IPPORT",               "7777")
epicsEnvSet("SYSTEM",               "SE-SEE")
epicsEnvSet("DEVICE",               "SE-LS336-004")
epicsEnvSet("PREFIX",               "$(SYSTEM):$(DEVICE)")
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(lakeshore336_DIR)db/")

# E3 Common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")

### load all db's
iocshLoad("$(lakeshore336_DIR)lakeshore336.iocsh", "PREFIX=$(PREFIX), IPADDR=$(IPADDR), IPPORT=$(IPPORT)")

### install SNL curves
seq install_curve, "P=$(PREFIX), CurvePrefix=File"

iocInit()
